<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multiple Form Buttons Example</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    
    <cfif structKeyExists(URL, 'actionCompleted')>
        <div class="alert">
            <cfif url.actionCompleted EQ "UploadedFile">
                Your file was successfully uploaded!
            <cfelseif url.actionCompleted EQ "EmailedFile">
                Your file was successfully uploaded and email to the system administrator!
            <cfelseif url.actionCompleted EQ "Nothing">
                Nothing was done, just letting you know!
            </cfif>
        </div>
    </cfif>
    <form method="post" action="form-example-process.cfm" enctype="multipart/form-data">
        <table class="table table-striped">
            <tr>
            <th>File</th>
            <th colspan="2">Select what you'd like to do with it!</th>
            </tr>
            <tr>
                <td>
                <input type="file" name="uploadFile" />
                </td>
                <td>
                <input type="submit" name="whatToDoWithFile" value="Upload File To Server" />
            </td>
            <td>
                <input type="submit" name="whatToDoWithFile" value="Upload File To Server and Email It To SiteAdmin" />
            </td>
            </tr>
        </table>
    </form>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>