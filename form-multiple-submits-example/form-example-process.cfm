<cfif structKeyExists(form, 'fieldnames') AND structKeyExists(form, 'whatToDoWithFile') AND structKeyExists(form, 'uploadFile')>

        <!--- This code will only run when a form is submitted to this page, and when the variable "submitButton" exists --->
        <!--- 
              This is where we will put in the ColdFusion that will handle the processing   
        --->
        <cfif form.whatToDoWithFile EQ "Upload File To Server">
            <!--- here we will upload the file to the uploads folder only --->
            <cffile action="upload" filefield="uploadFile" destination="#expandPath('/uploads/')#" nameconflict="makeunique" />
            <cfset uploadedFileName = cffile.serverfile />

            <!--- That's it... this processing is done now let's redirect the user back to the display page --->
            <cflocation url="form-example.cfm?actionCompleted=UploadedFile" addtoken="false" />
            
        <cfelseif form.whatToDoWithFile EQ "Upload File To Server and Email It To SiteAdmin">
            <!--- here we will upload the file to the email folder and then email the file to the user --->
            <cffile action="upload" filefield="uploadFile" destination="#expandPath('/emails/')#" nameconflict="makeunique" />
            <cfset uploadedFileName = cffile.serverfile />

            <!--- Now let's send an email with the file as an attachment --->
            <cfmail from="uploads@mysite.com" to="administrator@mysite.com" subject="A User Uploaded A File For You!">
                <cfmailparam file="#expandPath('/emails/' & uploadedFileName )#" />
            </cfmail>
            
            <!--- That's it... this processing is done now let's redirect the user back to the display page --->
            <cflocation url="form-example.cfm?actionCompleted=EmailedFile" addtoken="false" />
        <cfelse>
            <!--- Since no button matched what we wanted to do, just redirect back to the main page with an error --->
            <cflocation url="form-example.cfm?actionCompleted=Nothing" addtoken="false" />
        </cfif>

</cfif>